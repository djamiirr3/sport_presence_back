<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStudentActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eleve_activite', function(Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
           $table->foreign('user_id')->references('id')->on('asp_users')->nullOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eleve_activite', function(Blueprint $table) {
            $table->dropColumn(['user_id']);
        });
    }
}
