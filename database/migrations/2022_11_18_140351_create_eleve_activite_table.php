<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEleveActiviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eleve_activite', function (Blueprint $table) {
            $table->id();
            $table->foreignId('eleve_id')->references('id')->on('eleve')->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('activite_id')->constrained()->references('id')->on('activite')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eleve_activite');
    }
}
