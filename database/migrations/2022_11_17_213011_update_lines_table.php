<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ligne', function (Blueprint $table) {
            $table->unsignedBigInteger('ideleve')->change();
            $table->foreign('identete')->references('id')->on('entete')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('ideleve')->references('id')->on('eleve')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(DB::raw('ALTER TABLE ligne DROP FOREIGN KEY ligne_identete_foreign;'));
        DB::statement(DB::raw('ALTER TABLE ligne DROP INDEX ligne_identete_foreign'));
        DB::statement(DB::raw('ALTER TABLE ligne DROP FOREIGN KEY ligne_ideleve_foreign;'));
        DB::statement(DB::raw('ALTER TABLE ligne DROP INDEX ligne_ideleve_foreign'));
    }
}
