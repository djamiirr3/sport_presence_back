<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entete', function (Blueprint $table) {
           $table->time('enteteheure')->nullable()->change();
           $table->time('heurefin')->nullable()->change();
           $table->integer('transport')->nullable()->change();
           $table->string('niveau', 45)->nullable()->change();
           $table->string('championnat', 45)->nullable()->change();
           $table->date('entetedate')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
