<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eleve', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->change();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(DB::raw('SET foreign_key_checks = 0'));
        DB::statement(DB::raw('ALTER TABLE ligne DROP FOREIGN KEY ligne_ideleve_foreign;'));
        DB::statement(DB::raw('ALTER TABLE ligne DROP INDEX ligne_ideleve_foreign'));
        DB::statement(DB::raw('ALTER TABLE eleve DROP PRIMARY KEY; '));
        DB::statement(DB::raw('SET foreign_key_checks = 1'));
    }
}
