<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateEnteteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entete', function (Blueprint $table) {
            $table->foreign('idprof')->references('login')->on('asp_users')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(DB::raw('ALTER TABLE entete DROP FOREIGN KEY entete_idprof_foreign;'));
        DB::statement(DB::raw('ALTER TABLE entete DROP INDEX entete_idprof_foreign;'));
    }
}
