<?php

namespace App\Modules\Session\Http\Resources;

use App\Http\Resources\ChampionshipResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EquipmentResource;
use App\Http\Resources\InstallationResource;
use App\Http\Resources\LevelResource;
use App\Http\Resources\TransitResource;
use App\Modules\Line\Http\Resources\LineResource;
use App\Modules\User\Http\Resources\LoginResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SessionUpdateReource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "teacher" => $this->teacher->login,
            "start_date" => $this->entetedate,
            "start_time" => $this->enteteheure,
            "academy" => $this->Acad,
            "department" => new DepartmentResource($this->department),
            "city" => new CityResource($this->city),
            "installation" => new InstallationResource($this->installation),
            "equipment" => new EquipmentResource($this->equipment),
            "level" => new LevelResource($this->level),
            "championship" => $this->championnat,
            "transit" => new TransitResource($this->transit),
            "activity" => $this->activite,
            "accompagnateur" => new LoginResource($this->companion),
            "end_time" => $this->heurefin,
            "finished" => $this->terminer == 'Y',
            "enabled" => (bool) $this->show_on_app,
        ];
    }
}
