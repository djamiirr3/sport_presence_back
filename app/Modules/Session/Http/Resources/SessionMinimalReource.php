<?php

namespace App\Modules\Session\Http\Resources;

use App\Http\Resources\EquipmentResource;
use App\Modules\Line\Http\Resources\LineResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SessionMinimalReource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "teacher" => $this->idprof,
            "start_date" => $this->entetedate,
            "start_time" => $this->enteteheure,
            "activity" => $this->activite,
            "end_time" => $this->heurefin,
            "finished" => strtoupper($this->terminer) == 'Y',
            "enabled" => (bool) $this->show_on_app,
        ];
    }
}
