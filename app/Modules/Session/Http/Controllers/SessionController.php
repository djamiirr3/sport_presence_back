<?php

namespace App\Modules\Session\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EquipmentResource;
use App\Http\Resources\InstallationResource;
use App\Modules\Session\Http\Requests\FinishSessionRequest;
use App\Modules\Session\Http\Requests\SessionRequest;
use App\Modules\Session\Http\Requests\UpdateSessionRequest;
use App\Modules\Session\Http\Resources\SessionMinimalReource;
use App\Modules\Session\Http\Resources\SessionReource;
use App\Modules\Session\Http\Resources\SessionUpdateReource;
use App\Repositories\SessionRepository;
use Illuminate\Http\Request;

class SessionController extends Controller
{

    private $sessions;

    /**
     * SessionController constructor.
     * @param $sessions
     */
    public function __construct(SessionRepository $sessions)
    {
        $this->sessions = $sessions;
    }


    public function getAcademies()
    {
        $data = $this->sessions->getAcademies();
        return response()->json($data);
    }

    public function getDepartments($academy)
    {
        $data = $this->sessions->getDepartments($academy);
        return response()->json(DepartmentResource::collection($data));
    }

    public function getCities($department)
    {
        $data = $this->sessions->getCities($department);
        return response()->json(CityResource::collection($data));
    }

    public function getInstallations($cominsee)
    {
        $data = $this->sessions->getInstallations($cominsee);
        return response()->json(InstallationResource::collection($data));
    }

    public function getEquipments($installation)
    {
        $data = $this->sessions->getEquipments($installation);
        return response()->json(EquipmentResource::collection($data));
    }

    public function getMisc()
    {
        $data = $this->sessions->getMisc();
        return response()->json($data);
    }

    public function addSession(SessionRequest $rq)
    {
        $data = $this->sessions->postSession($rq->validated());
        return response()->json(new SessionReource($data));
    }

    public function getSessions()
    {
        $data = $this->sessions->getSessions();
        return response()->json(SessionMinimalReource::collection($data));
    }


    public function getSession($id)
    {
        $data = $this->sessions->getSession($id);
        return response()->json(new SessionReource($data));
    }

    public function updateSession($id, SessionRequest $rq)
    {
        $data = $this->sessions->updateSession($id, $rq);
        return response()->json(is_null($data) ? new \stdClass() : new SessionUpdateReource($data));
    }


    public function finishSession($id, FinishSessionRequest $rq)
    {
        $this->sessions->finishSession($id, $rq->validated());
        return response()->json(new \stdClass());
    }



}
