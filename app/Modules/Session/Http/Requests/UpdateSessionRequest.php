<?php

namespace App\Modules\Session\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'date',
            'academy' => 'string|exists:acad,academie',
            'comm' => 'string|exists:lieu,ComInsee',
            "department" => 'string|exists:acad,DepCode',
            "city" => 'string|exists:lieu,ComLib',
            "installation.id" => "string|exists:installation,InsNumeroIstall",
            "installation.name" => "string|exists:installation,InsNom",
            "equipment" => 'string|exists:equipement,EquipementId',
            "level" => 'string|exists:competition,id',
            "championship" => 'string|exists:championnat,intitulé',
            "transit" => 'integer|exists:transport,id',
            "activity" => 'string|exists:activite,activite',
            'end_time' => 'date|after:date',
        ];
    }
}
