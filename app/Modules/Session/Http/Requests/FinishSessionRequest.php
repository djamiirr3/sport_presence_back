<?php

namespace App\Modules\Session\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FinishSessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'end_time' => 'required|date'
        ];
    }
}
