<?php

namespace App\Modules\Session\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'nullable|date',
            'academy' => 'required|string|exists:acad,academie',
            'city_id' => 'required|string|exists:lieu,ComInsee',
            "department" => 'required|string|exists:acad,DepCode',
            "city" => 'required|string|exists:lieu,ComLib',
            "companion" => 'nullable|string|exists:asp_users,login',
            "installation" => "required|string|exists:installation,InsNumeroIstall",
            "equipment" => 'required|string|exists:equipement,EquipementId',
            "level" => 'nullable|string|exists:competition,id',
            "championship" => 'nullable|string|exists:championnat,intitulé',
            "transit" => 'nullable|integer|exists:transport,id',
            "activity" => 'required|string|exists:activite,activite',
            'end_time' => 'nullable|date|after:start_time',
            "start_time" => 'nullable|date',
            'finished' => 'required|boolean'
        ];
    }
}
