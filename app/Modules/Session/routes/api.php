<?php

use App\Modules\Session\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Route;

Route::group(['module' => 'Session', 'namespace' => 'App\Modules\Session\Controllers', 'prefix' => "api/sessions", 'middleware' => ['auth:api']], function () {
    Route::post('', [SessionController::class, 'addSession']);
    Route::get('', [SessionController::class, 'getSessions']);
    Route::get('/academies', [SessionController::class, 'getAcademies']);
    Route::get('/departments/{academy}', [SessionController::class, 'getDepartments']);
    Route::get('/cities/{department}', [SessionController::class, 'getCities']);
    Route::get('/installations/{cominses}', [SessionController::class, 'getInstallations']);
    Route::get('/equipments/{installation}', [SessionController::class, 'getEquipments']);
    Route::get('/misc', [SessionController::class, 'getMisc']);
    Route::get('/{id}', [SessionController::class, 'getSession']);
    Route::put('/{id}', [SessionController::class, 'updateSession']);
    Route::delete('/{id}', [SessionController::class, 'finishSession']);
});
