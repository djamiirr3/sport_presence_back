<?php

namespace App\Modules\Session\Models;

use App\Models\Academy;
use App\Models\Activity;
use App\Models\Championship;
use App\Models\Equipment;
use App\Models\Installation;
use App\Models\Level;
use App\Models\Place;
use App\Models\Transit;
use App\Modules\Line\Models\Line;
use App\Modules\Student\Models\Student;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Session extends Model
{
    use HasFactory;

    protected $table = "entete";

    public $timestamps = false;

    public function teacher() {
        return $this->hasOne(User::class, 'login', 'idprof');
    }

    public function lines() {
        return $this->hasMany(Line::class, "identete");
    }

    public function equipment() {
        return $this->belongsTo(Equipment::class, 'equipement', 'EquipementId');
    }

    public function level() {
        return $this->belongsTo(Level::class, 'niveau');
    }

    public function transit() {
        return $this->belongsTo(Transit::class, 'transport');
    }

    public function city() {
        return $this->belongsTo(Place::class, 'ville', 'ComLib');
    }

    public function installationObj() {
        return $this->belongsTo(Installation::class, 'installation');
    }

    public function department() {
        return $this->belongsTo(Academy::class, 'departement', 'DepCode');
    }

    public function activity() {
        return $this->belongsTo(Activity::class, 'activite', 'activite');
    }

    public function companion() {
        return $this->belongsTo(User::class, 'accompagnateur', 'login');
    }

}
