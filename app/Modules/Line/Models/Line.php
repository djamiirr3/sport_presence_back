<?php

namespace App\Modules\Line\Models;

use App\Models\Academy;
use App\Models\Installation;
use App\Models\Place;
use App\Modules\Session\Models\Session;
use App\Modules\Student\Models\Student;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    use HasFactory;

    protected $table = 'ligne';

    public $timestamps = false;


    protected $primaryKey = "idligne";

    public function session() {
        return $this->belongsTo(Session::class, 'identete');
    }

    public function student() {
        return $this->belongsTo(Student::class, 'ideleve');
    }

    public function academy() {
        return $this->belongsTo(Academy::class, 'academie');
    }
}
