<?php

use App\Modules\Line\Http\Controllers\LineController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['module' => 'Line', 'namespace' => 'App\Modules\Line\Controllers', 'prefix' => "api/lines", 'middleware' => ['auth:api']], function () {
    Route::put('/', [LineController::class, 'updateLines']);
});
