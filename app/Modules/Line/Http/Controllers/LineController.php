<?php

namespace App\Modules\Line\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Line\Http\Requests\UpdateLineRequest;
use App\Modules\Line\Http\Resources\LineResource;
use App\Repositories\LineRepository;
use Illuminate\Http\Request;

class LineController extends Controller
{

    private $lines;

    /**
     * @param $lines
     */
    public function __construct(LineRepository $lines)
    {
        $this->lines = $lines;
    }


    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("Line::welcome");
    }

    public function updateLines(UpdateLineRequest $rq) {
        $data = $this->lines->updateLines($rq->validated());
        return response()->json(LineResource::collection($data));
    }
}
