<?php

namespace App\Modules\Line\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.id' => 'required|exists:ligne,idligne',
            '*.convoque' => 'required|boolean',
            '*.comment' => 'nullable|string',
            '*.present' => 'nullable|boolean',
            '*.start_time' => 'nullable|date',
            '*.end_time' => 'nullable|date'
        ];
    }

}
