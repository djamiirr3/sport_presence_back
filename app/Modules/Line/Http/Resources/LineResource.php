<?php

namespace App\Modules\Line\Http\Resources;

use App\Modules\Student\Http\Resources\StudentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->idligne,
            "student" => new StudentResource($this->student),
            "present" => strtoupper($this->presence) == 'Y',
            "entry_time" => $this->heurearrive,
            "exit_time" => $this->heuredepart,
            "comment" => $this->commentaire,
            "convoque" => $this->absent == 'Y',
        ];
    }
}
