<?php

namespace App\Modules\Establishment\Models;

use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    use HasFactory;

    protected $table = 'etablissement';

    public function users() {
        return $this->hasMany(User::class, 'établissement', 'idetablissement');
    }
}
