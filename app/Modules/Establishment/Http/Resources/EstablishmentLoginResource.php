<?php

namespace App\Modules\Establishment\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EstablishmentLoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->idetablissement,
            'name' => $this->etablissement,
            'academy' => $this->acad,
            'address' => $this->adresse,
            'city' => $this->academie,// Place::where('ComInsee', $this->academie)->get()->first()->ComLib,
            'department' => $this->departement,
        ];
    }
}
