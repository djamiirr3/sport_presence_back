<?php

use App\Modules\Student\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

Route::group(['module' => 'Student', 'namespace' => 'App\Modules\Student\Controllers', 'prefix' => "api/students", 'middleware' => ['auth:api']], function () {
    Route::get('/', [StudentController::class, 'getAll']);
    Route::get('/divs', [StudentController::class, 'getDivisions']);
    Route::get('/cats', [StudentController::class, 'getCategories']);
    Route::get('/activities', [StudentController::class, 'getActivities']);
    Route::put('/{id}', [StudentController::class, 'updateStudent']);
});
