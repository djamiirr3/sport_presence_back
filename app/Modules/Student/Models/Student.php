<?php

namespace App\Modules\Student\Models;

use App\Models\Activity;
use App\Modules\Line\Models\Line;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    use HasFactory;

    protected $table = 'eleve';

    public $timestamps = false;

    public function scopeFindByName($query,$name) {
        // Concat the name columns and then apply search query on full name
        $query->where(DB::raw(
            "REPLACE(
                /* CONCAT will concat the columns with defined separator */
                CONCAT(
                    /* COALESCE operator will handle NUll values as defined value. */
                    COALESCE(Nom,''),' ',
                    COALESCE(Prenom,''),' '
                ),
            '  ',' ')"
        ),
            'like', '%' . $name . '%');
    }

    public function lines($id) {
        return $this->hasMany(Line::class, "ideleve")->whereIdligne($id);
    }

    public function activities() {
        return $this->belongsToMany(Activity::class, 'eleve_activite', 'eleve_id', 'activite_id',);
    }

}
