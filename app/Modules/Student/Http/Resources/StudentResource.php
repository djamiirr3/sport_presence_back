<?php

namespace App\Modules\Student\Http\Resources;

use App\Http\Resources\ActivityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'id_student' => $this->ideleve,
            'ine' => $this->Ine,
            'name' => $this->Nom,
            'last_name' => $this->Prenom,
            'gender' => $this->Sexe,
            'birth_date' => $this->Date_de_naissance,
            'division' => $this->Division,
            'category' => $this->Categorie,
            'subscribed' => $this->Inscrit == 'Y',
            'email' => $this->Email,
            'phone' => $this->Mobile,
            'activities' => ActivityResource::collection($this->activities)
        ];
    }
}
