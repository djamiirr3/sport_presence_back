<?php

namespace App\Modules\Student\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\ActivityResource;
use App\Modules\Session\Http\Requests\UpdateSessionRequest;
use App\Modules\Student\Http\Requests\UpdateStudentRequest;
use App\Modules\Student\Http\Resources\DivisionResource;
use App\Modules\Student\Http\Resources\StudentResource;
use App\Repositories\StudentRepository;
use Illuminate\Http\Response;

class StudentController extends Controller
{

    private $students;

    /**
     * StudentController constructor.
     * @param $students
     */
    public function __construct(StudentRepository $students)
    {
        $this->students = $students;
    }


    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("Student::welcome");
    }

    public function getAll()
    {
        $data = $this->students->getAll();
        return response()->json(StudentResource::collection($data));
    }

    public function getDivisions()
    {
        $data = $this->students->getDivisions();
        return response()->json($data);
    }

    public function getCategories()
    {
        $data = $this->students->getCategories();
        return response()->json($data);
    }

    public function getActivities()
    {
        $data = $this->students->getActivities();
        return response()->json(ActivityResource::collection($data));
    }

    public function updateStudent($id, UpdateStudentRequest $rq)
    {
        $data = $this->students->updateStudent($id, $rq->validated());
        return response()->json(is_null($data) ? ['message' => 'User not found'] : new StudentResource($data), is_null($data) ? Response::HTTP_NOT_FOUND : Response::HTTP_OK);
    }


}
