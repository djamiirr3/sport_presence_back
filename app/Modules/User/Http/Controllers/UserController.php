<?php

namespace App\Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\User\Http\Requests\LoginRequest;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{

    private $users;

    /**
     * UserController constructor.
     * @param $users
     */
    public function __construct(UsersRepository $users)
    {
        $this->users = $users;
    }


    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("User::welcome");
    }

    public function login(LoginRequest $rq) {
        $data = $this->users->login($rq->validated());
        return response()->json(!is_null($data) ? $data : ['message' => 'Login failed'], !is_null($data) ? Response::HTTP_OK : Response::HTTP_UNAUTHORIZED);
    }
}
