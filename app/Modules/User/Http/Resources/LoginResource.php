<?php

namespace App\Modules\User\Http\Resources;

use App\Modules\Establishment\Http\Resources\EstablishmentLoginResource;
use App\Modules\User\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Psy\CodeCleaner\AssignThisVariablePass;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'name' => $this->name,
            'email' => $this->email,
            'level' => User::$LEVELS[$this->Niveau],
            'establishment' => new EstablishmentLoginResource($this->establishment),
        ];
    }
}
