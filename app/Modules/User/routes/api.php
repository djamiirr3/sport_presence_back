<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['module' => 'User', 'namespace' => 'App\Modules\User\Controllers', 'prefix' => "api/users"], function() {

    Route::post('login', [\App\Modules\User\Http\Controllers\UserController::class, 'login']);
    Route::group(['middleware' => ['auth:api']], function() {
        Route::get('/link', [\App\Modules\User\Http\Controllers\UserController::class, 'getMyBranchData']);
        Route::post('/logout', function () {
            Auth::user()->token()->revoke();
            return response()->json();
        });
    });
});
