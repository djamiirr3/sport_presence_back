<?php

namespace App\Repositories;

use App\Modules\User\Http\Resources\LoginResource;
use App\Modules\User\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class UsersRepository.
 *
 * @package namespace App\Repositories;
 */
class UsersRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }


    public function login($rq) {
        $user = User::with(['establishment'])->whereLogin($rq['email'])->get()->first();

        if (is_null($user) || !in_array($user->Niveau, [0, 2]) || $user->pswd != $rq['password']) {
            return null;
        }

        return ['user' => new LoginResource($user),
            'token' => $user->createToken('global')->accessToken];
    }

}
