<?php

namespace App\Repositories;

use App\Models\Activity;
use App\Models\StudentActivity;
use App\Modules\Student\Models\Student;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class StudentRepository.
 *
 * @package namespace App\Repositories;
 */
class StudentRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Student::class;
    }


    public function getAll() {
        $page = request('page', 0);
        $limit = request('limit', 20);
        $division = request('division');
        $gender = request('gender');
        $category = request('category');
        $name = request('name');
        $activity = request('activity');
        $subscribed = request('subscribed');
        $students = Student::with(['activities'])->where('IdEtablisement', User::find(Auth::id())->établissement)->orderBy('nom');
        if (isset($division)) {
            $students = $students->where('Division', $division);
        }
        if (isset($gender)) {
            $students = $students->where('Sexe', strtoupper($gender));
        }
        if (isset($category)) {
            $students = $students->where('Categorie', $category);
        }
        if (isset($name)) {
            $students = $students->findByName($name);
        }
        if (isset($subscribed)) {
            $students = $students->where('Inscrit', $subscribed == true ? 'Y' : null);
        } if (isset($activity)) {
            $students = $students->whereHas('activities', function($q) use ($activity) {
                $q->where(DB::raw('activite.id'), '=',  $activity);
            });
        }

        return $students->limit($limit)->offset($limit*($page))->get();
    }

    public function getActivities() {
        return Activity::get();
    }

    public function getDivisions() {
        $divisions = DB::select(DB::raw('select * from divisions'));
        $data = array();
        foreach ($divisions as $div) {
            $data[] = $div->Division;
        }
        return $data;
    }

    public function getCategories() {
        $categories = DB::select(DB::raw('select * from categories'));
        $data = array();
        foreach ($categories as $cat) {
            $data[] = $cat->categorie;
        }
        return $data;
    }

    public function updateStudent($id, array $rq)
    {
        try {
            $student = Student::with(['activities'])->findOrFail($id);
        } catch (\Throwable $e) {
            return null;
        }
        //$student = new Student();
        $activity = Activity::find($rq['activity_id']);
        if ($student->activities->contains($activity)) {
            $student->activities()->detach($activity);
            $student->save();
        } else {
            $pivot = new StudentActivity();
            $pivot->user()->associate(Auth::user());
            $pivot->student()->associate($student);
            $pivot->activity()->associate($activity);
            $pivot->save();
        }


        return Student::with(['activities'])->findOrFail($id);
    }

}
