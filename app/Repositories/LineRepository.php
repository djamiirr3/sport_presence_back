<?php

namespace App\Repositories;

use App\Mail\DispatchMail;
use App\Models\Place;
use App\Modules\Line\Models\Line;
use App\Modules\Session\Models\Session;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mavinoo\Batch\Batch;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use function PHPUnit\Framework\isEmpty;

/**
 * Class LineRepository.
 *
 * @package namespace App\Repositories;
 */
class LineRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Line::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function updateLines(array $rqs)
    {
        $ids = array();
        $lines = array();
        foreach ($rqs as $rq) {
            $lines[] = $rq['id'];
        }

        $lines = Line::with(['student'])->whereIn('idligne', $lines)->get();
        foreach ($rqs as &$rq) {
            $rq['absent'] = $rq['convoque'] == true ? 'Y' : 'N';
            unset($rq['convoque']);
            if (is_null($rq['comment'])) {
                $rq['comment'] = "";
            }
            $rq['commentaire'] = $rq['comment'];
            unset($rq['comment']);
            $line = $lines->find($rq['id']);
            $rq['idligne'] = $rq['id'];
            if ($rq['absent'] == 'Y' && $line->absent != 'Y') {
                $ids[] = $rq['id'];
            } else if ($rq['absent'] == 'N' && $line->absent == 'Y') {
                unset($rq['absent']);
            }
            unset($rq['id']);
            if (!is_null($rq['start_time'])) {
                $rq['heurearrive'] = Carbon::parse($rq['start_time'])->toTimeString();
            } else {
                $rq['heurearrive'] = null;
            }

            if (!is_null($rq['end_time'])) {
                $rq['heuredepart'] = Carbon::parse($rq['end_time'])->toTimeString();
            } else {
                $rq['heuredepart'] = null;
            }
            if (!is_null($rq['present'])) {
                if (!$rq['present']) {
                    $rq['heurearrive'] = null;
                    $rq['heuredepart'] = null;
                }
                $rq['presence'] = $rq['present'] ? 'Y' : 'N';
            } else {
                $rq['heurearrive'] = null;
                $rq['heuredepart'] = null;
                $rq['presence'] = 'N';
            }
            unset($rq['present']);
            unset($rq['end_time']);
            unset($rq['start_time']);
        }

        if (!empty($ids)) {
            $lines = Line::with(['student'])->whereIn('idligne', $ids)->get();
            $session = Session::with(['installationObj'])->find($lines->first()->identete);
            foreach ($lines as $sl) {
                if (!is_null($sl->student->Email)) {
                    Mail::to($sl->student->Email)->queue(
                        new DispatchMail(
                            $sl->student->Nom . ' ' . $sl->student->Prenom,
                            $session->activite,
                            $session->installationObj->InsNom,
                            Carbon::parse($session->entetedate)->format('d/m/Y'),
                            Carbon::parse($session->enteteheure)->format('H:i'),
                            $session->ville
                        )
                    );
                }
            }
        }

        $batch = new Batch($this->app->make(DatabaseManager::class));
        $batch->update(new Line, $rqs, 'idligne');
        $ids = array();
        foreach ($rqs as $rs) {
            $ids [] = $rs['idligne'];
        }
        return Line::with(['student'])->whereIn('idligne', $ids)->get();
    }

}
