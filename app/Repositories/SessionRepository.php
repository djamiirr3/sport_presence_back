<?php

namespace App\Repositories;

use App\Http\Resources\ActivityResource;
use App\Http\Resources\ChampionshipResource;
use App\Http\Resources\LevelResource;
use App\Http\Resources\TransitResource;
use App\Mail\DispatchMail;
use App\Models\Academy;
use App\Models\Championship;
use App\Models\Equipment;
use App\Models\Installation;
use App\Models\Level;
use App\Models\Models\Department;
use App\Models\Place;
use App\Models\Activity;
use App\Models\Transit;
use App\Modules\Session\Models\Session;
use App\Modules\Line\Models\Line;
use App\Modules\Student\Models\Student;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Mavinoo\Batch\Batch;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class SessionRepository.
 *
 * @package namespace App\Repositories;
 */
class SessionRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Session::class;
    }


    public function getAcademies()
    {
        $academies = Academy::distinct('academie')->orderBy('academie')->get('academie');
        $data = array();
        foreach ($academies as $academy) {
            $data[] = $academy['academie'];
        }
        return $data;
    }

    public function getDepartments($academy)
    {
        $departments = Academy::whereAcademie($academy)->orderBy('DepLib')->get(['DepCode', "DepLib"]);
        return $departments;

    }

    public function getCities($department)
    {
        $cities = Place::where('DepCode', $department)->distinct('ComLib')->orderBy('ComLib')->get(['ComLib', "ComInsee"]);

        return $cities;
    }

    public function getInstallations($cominsee)
    {
        return Installation::where('ComInsee', $cominsee)->orderBy('InsNom')->get(['InsNumeroIstall', 'InsNom']);
    }

    public function getEquipments($installation)
    {
        return Equipment::where('InsNumeroIstall', $installation)->orderBy('EquiNom')->get(['EquiNom', 'EquipementId']);
    }

    public function getMisc()
    {
        $data = array();
        $data['levels'] = LevelResource::collection(Level::get());
        $data['championships'] = ChampionshipResource::collection(Championship::get());
        $data['activities'] = ActivityResource::collection(Activity::get());
        $data['transits'] = TransitResource::collection(Transit::get());
        $data['accompanist'] = User::get(['login', 'id', 'name']);

        return $data;
    }

    public function postSession($rq)
    {
        $session = new Session();
        $user = User::find(Auth::id());
        $session->IdProf = $user->login;
        $session->entetedate = isset($rq['data']) ? Carbon::parse($rq['date'])->toDateString() : Carbon::now()->toDateString();
        $session->enteteheure = isset($rq['start_time']) ? Carbon::parse($rq['start_time'])->toTimeString() : Carbon::now()->toTimeString();
        $session->Acad = $rq['academy'];
        $session->academie = $rq['city_id'];
        $session->departement = $rq['department'];
        $session->ville = $rq['city'];
        $session->installation = $rq['installation'];
        $session->equipement = $rq['equipment'];
        $session->niveau = $rq['level'];
        $session->championnat = $rq['championship'];
        $session->transport = $rq['transit'];
        $session->activite = $rq['activity'];
        $session->accompagnateur = '';
        $session->show_on_app = true;
        $session->heurefin = isset($rq['end_time']) ? Carbon::parse($rq['end_time'])->toTimeString() : null;
        $session->terminer = $rq['finished'] == true ? 'Y' : 'N';
        $session->idetablissement = $user->établissement;
        if (isset($rq['companion'])) {
            $session->accompagnateur = $rq['companion'];
        }

        $session->save();

        $activity = Activity::whereActivite($rq['activity'])->get()->first();

        $students = $this->getActivityStudents($activity);

        $stds = array();

        foreach ($students as $student) {
            $stds[] = ['identete' => $session->id, 'ideleve' => $student->id, 'presence' => 'N', 'commentaire' => '',
                'idetablissement' => $session->idetablissement];
        }

        Line::insert($stds);

        $session = Session::with(['lines.student.activities', 'equipment.installation', 'transit', 'level', 'city', 'department', 'companion'])->find($session->id);

        $session->installation = $session->equipment->installation;


        //$session->level = Level::find($rq['level']);
        //$session->activity = Activity::where('activite', $rq['activity'])->get()->first();
        //$session->transit = Transit::find($rq['transit']);


        return $session;
    }

    public function getSessions()
    {
        $page = request('page', 0);
        $limit = request('limit', 20);
        $student = request('student');
        $date = request('date');
        $startTime = request('start_time');
        $endTime = request('end_time');
        $activity = request('activity');
        $session = Session::where('terminer', request('active', 'true') == 'true' ? '<>' : '=', 'Y')
            ->whereIdprof(User::find(Auth::id())->login);

        if (!is_null($activity)) {
            $session = $session->whereActivite($activity);
        }

        if (!is_null($date)) {
            $session = $session->whereEntetedate(Carbon::parse($date));
        }

        if (!is_null($startTime)) {
            $session = $session->where(DB::raw('TIME_FORMAT(enteteheure, "%H:%i")'), urldecode($startTime));
        }

        if (!is_null($endTime)) {
            $session = $session->where(DB::raw('TIME_FORMAT(heurefin, "%H:%i")'), urldecode($endTime));
        }

        if (!is_null($student)) {
            $session = $session->join('ligne', 'entete.id', '=', 'ligne.identete')
                ->join('eleve', 'eleve.id', '=', 'ligne.ideleve')->where(DB::raw(
                    "REPLACE(CONCAT(COALESCE(eleve.Nom,''),' ',COALESCE(eleve.Prenom,''),' '),'  ',' ')"
                ), 'like', '%' . $student . '%');
        }

        $session = $session->orderByDesc('entetedate')->orderBy('enteteheure')->limit($limit)->offset($limit * $page)->groupBy('entete.id')->get(['entete.*']);

        return $session;
    }

    public function getSession($id)
    {
        $session = Session::with(['lines.student.activities', 'equipment.installation', 'transit', 'level', 'city', 'department', 'companion'])->find($id);
        $session->installation = $session->equipment->installation;
        return $session;
    }

    public function updateSession($id, $rq)
    {
        try {
            $session = Session::findOrFail($id);
        } catch (\Throwable $e) {
            return null;
        }

        if ($rq['activity'] != $session->activite) {
            Line::whereHas('session', function ($q) use ($session) {
                $q->whereIdentete($session->id);
            })->delete();

            $stds = array();

            $students = $this->getActivityStudents(Activity::whereActivite($rq['activity'])->get()->first());

            foreach ($students as $student) {
                $stds[] = ['identete' => $session->id, 'ideleve' => $student->id, 'presence' => 'N', 'commentaire' => '',
                    'idetablissement' => $session->idetablissement];
            }

            Line::insert($stds);
        }

        $session->entetedate = Carbon::parse($rq['date'])->toDateString();
        $session->enteteheure = is_null($rq['start_time']) ? null : Carbon::parse($rq['start_time'])->toTimeString();
        $session->Acad = $rq['academy'];
        $session->academie = $rq['city_id'];
        $session->departement = $rq['department'];
        $session->ville = $rq['city'];
        $session->installation = $rq['installation'];
        $session->equipement = $rq['equipment'];
        $session->niveau = !is_null($rq['level']) ? $rq['level'] : '';
        $session->championnat = !is_null($rq['championship']) ? $rq['championship'] : '';
        $session->transport = $rq['transit'];
        $session->activite = $rq['activity'];
        $session->accompagnateur = '';
        $session->heurefin = is_null($rq['end_time']) ? null : Carbon::parse($rq['end_time'])->toTimeString();
        $session->terminer = $rq['finished'] == true ? 'Y' : 'N';
        if (isset($rq['companion'])) {
            $session->accompagnateur = $rq['companion'];
        }
        /*
        if (isset($rq['comm'])) {
            $session->accompagnateur = '';
        }*/

        $session->save();

        $session = Session::with(['equipment.installation', 'transit', 'level', 'department', 'teacher', 'companion', 'city'])->find($id);

        $session->installation = $session->equipment->installation;

        return $session;
    }

    public function finishSession($id, $rq)
    {
        try {
            $session = Session::with(['lines' => function ($q) {
                return $q->where('presence', 'Y')->whereNull('heuredepart');
            }, 'installationObj', 'lines.student'])->findOrFail($id);
        } catch (\Throwable $e) {
            return;
        }

        if ($session->terminer == 'Y') {
            return null;
        }

        $endTime = Carbon::parse($rq['end_time'])->toTimeString();

        $session->terminer = 'Y';
        $session->heurefin = $endTime;

        $lines = $session->lines;
        $lns = [];


        foreach ($lines as &$line) {
            $line->heuredepart = $endTime;
            if (!is_null($line->student->Email)) {
                Mail::to($line->student->Email)->queue(
                    new DispatchMail(
                        $line->student->Nom . ' ' . $line->student->Prenom,
                        $session->activite,
                        $session->installationObj->InsNom,
                        Carbon::parse($session->entetedate)->format('d/m/Y'),
                        Carbon::parse($session->enteteheure)->format('H:i'),
                        $session->ville,
                        Carbon::parse($session->heuredepart)->format('H:i'),
                        false,
                    )
                );
            }
            unset($line->student);
            $lns[] = $line;
        }

        $lns = collect($lns);

        //$batch = new Batch($this->app->make(DatabaseManager::class));
        batch()->update(new Line, $lns->toArray(), 'idligne');


        $session->save();
    }

    private function getActivityStudents($activity)
    {
        return Student::whereHas('activities', function ($q) use ($activity) {
            $q->where(DB::raw('activite.id'), '=', $activity->id);
        })->where('IdEtablisement', User::find(Auth::id())->établissement)->get();
    }


}
