<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DispatchMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $activity;
    public $installation;
    public $date;
    public $time;
    public $endTime;
    public $city;
    private $toDispatch;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $activity, $installation, $date, $time, $city, $endTime = null, $toDispatch = true)
    {
        $this->activity = $activity;
        $this->name = $name;
        $this->installation = $installation;
        $this->date = $date;
        $this->time = $time;
        $this->city = $city;
        $this->toDispatch = $toDispatch;
        $this->endTime = $endTime;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Convocation - Sport Presence')->markdown('emails.' . ($this->toDispatch ? 'dispatched' : 'presence'));
    }
}
