<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;

    protected $primaryKey = 'EquipementId';

    protected $table = "equipement";

    public $timestamps = false;

    public function installation() {
        return $this->belongsTo(Installation::class, 'InsNumeroIstall');
    }
}
