<?php

namespace App\Models;

use App\Modules\Student\Models\Student;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $table = 'activite';

    public function student() {
        return $this->belongsToMany(Student::class, 'eleve_activite', 'activite_id', 'eleve_id', );
    }
}
