<?php

namespace App\Models;

use App\Modules\Student\Models\Student;
use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentActivity extends Model
{
    use HasFactory;

    protected $table = "eleve_activite";
    public $timestamps = false;

    public function student() {
        return $this->belongsTo(Student::class, 'eleve_id');
    }

    public function activity() {
        return $this->belongsTo(Activity::class, 'activite_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
