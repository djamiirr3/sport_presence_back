<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Installation extends Model
{
    use HasFactory;

    protected $primaryKey = 'InsNumeroIstall';

    protected $table = 'installation';

    public function equipments() {
        return $this->hasMany(Equipment::class, 'InsNumeroIstall');
    }
}
