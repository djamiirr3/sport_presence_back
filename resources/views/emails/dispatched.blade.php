@component('mail::message')
    <img src="https://alefmediagroup.com/amir/public/storage/logo.png" alt="logo"
         style="margin: 0 auto; width: 50%; margin-bottom: 20px; display: flex"/><br/>
    Bonjour <strong>{{$name}}</strong>.<br/><br/>
    Vous êtes convoqué pour votre cours de <strong>{{$activity}} </strong> le
    <strong>{{$date}}</strong> à
    <strong>{{$time}}</strong>
    au <strong>{{$installation}}</strong> à <strong>{{$city}}</strong>.
    <br>
    Merci,<br><br>
    {{ config('app.name') }}

@endcomponent
